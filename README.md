# Custom PHP Website Scraper
Written By Nathaniel Waddell | contact@nathanielwaddell.com

The following is an extendable PHP commandline application for scraping websites for different entities. At this time, it is only setup for scraping vehicles from Carvana.

The application has dynamic class loading, although PSR-4 namespacing has not been setup at this time. To complete this, I would add composer's autoloader to the `app/boot.php`. It also has a lightweight custom ORM written to allow future Websites and Entities to be added to the scraper tool. This could essentially be setup on a server with crontab to perform regular queries of a series of websites and entities.

To setup this application, set the database values in `app/config.json` to point to a fresh mysql installation. On the first run, the application will determine that there are no existing tables, and run the create table SQL file.

## Running the scraping application

### Command Syntax
``` bash
php scraper.php {command} {provider} {startPage} {endPage}
```

### Example
``` bash
php scraper.php scrape carvana
```

The application only requires the `command` and `provider` to execute. It will, by default, only scrape page one, and end on page one, for a total of one pages. You can adjust the startPage and endPage values to effectively control the pagination.

## Viewing Reports from the application

There is some basic statistics available via commandline, so you don't need to use your DBMS to see that this is working correctly.

### Print Record Counts for Tables

```bash
php scraper.php report {table} {options}
```

The only table currently available is `vehicles`. With the option `count`, the response is count-only. Otherwise, without that option, it will print the records into the cli in json.

## Upgrades that would be nice to see here

- PSR-4 Namespacing
- Checks for existing records (running the same page will just create duplicate records at this point)
- Would upgrade to using the .env library if building something business-grade
- Would figure out which of the many payloads used by the site updates the vehicle inventory so the entire json is not required for each page scraped
- Would create an option for the command to determine the pagination location from the json, and automatically walk the entire car library with some kind of rate limiting to prevent throwing flags