<?php

require_once (__DIR__ . '/app/boot.php');

if (!array_key_exists(1, $argv)) {
    throw new \Exception('Please enter the desired command you wish to run.');
}

switch ($argv[1]) {
    case 'scrape':
        $scraper = new ScrapeInventory($argv);
        echo $scraper->scrape();
        break;
    case 'report':
        $reporter = new ReportInventory($argv);
        echo $reporter->report();
        break;
    default:
        throw new \Exception(sprintf('Unknown command %s!', $argv[1]));
}

echo "\n";