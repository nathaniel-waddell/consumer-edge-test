<?php

class Carvana implements ProviderInterface
{
    const STARTING_DELIMETER = 'window.__PRELOADED_STATE__ = ';
    const ENDING_DELIMETER = 'window.deviceData = ';

    const URL = 'https://www.carvana.com/cars?email-capture=&page=';

    public function fetchInventoryByPage(int $page_id) : array
    {
        $pageData = Curl::fetch(self::URL . $page_id);


        $data = Strings::getFromBetweenTwoStrings($pageData,
            self::STARTING_DELIMETER,
            self::ENDING_DELIMETER
        );

        $json = json_decode($data, true);
        if (!array_key_exists('v2/inventory', $json) || !array_key_exists('vehicles', $json['v2/inventory'])) {
            return [];
        }

        $vehicles = [];
        foreach ($json['v2/inventory']['vehicles'] as $vehicleData) {
            $vehicle = new Vehicle();

            if (empty($vehicleData['make'])) {
                continue;
            }

            $vehicle->setVehicleId((int)$vehicleData['vehicleId']);
            $vehicle->setVin($vehicleData['stockNumber'] . $vehicleData['vehicleId']);
            $vehicle->setMake($vehicleData['make']);
            $vehicle->setModel($vehicleData['model']);
            $vehicle->setMileage((int)$vehicleData['mileage']);
            $vehicle->setPrice((float)$vehicleData['price']);

            $vehicles[] = $vehicle;
        }

        return $vehicles;
    }

    function saveInventory(array $vehicle, mysqli $db) : bool
    {
        // Persist a single vehicle record to the database
        // Return a boolean indicating success or failure
        return true;
    }
}