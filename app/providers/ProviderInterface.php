<?php

interface ProviderInterface
{
    public function fetchInventoryByPage(int $page_id) : array;

    public function saveInventory(array $vehicle, mysqli $db) : bool;
}