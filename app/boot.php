<?php

declare(strict_types = 1);

// Plug in composer if relevant
//require(__DIR__ . "/../vendor/autoload.php");

$directories = [
    'commands',
    'config',
    'entities',
    'functions',
    'providers'
];

// This is a poor way of doing this, but loading the files in the order needed
function loadFiles(string $file, string $path, string $type = null) {
    if ($file === '.' || $file === '..') {
        return;
    }
    $filepath = sprintf('%s/%s', $path, $file);
    $extension = pathinfo($filepath, PATHINFO_EXTENSION);
    if ($extension === 'php') {
        switch ($type) {
            case 'interface':
                if (substr($file, 0, -strlen(ucfirst('interface') . '.php'))) {
                    require($filepath);
                }
                break;
            case 'abstract':
                if (substr($file, 0, -strlen(ucfirst('interface') . '.php'))) {
                    return;
                }
                if (substr($file, strlen(ucfirst('abstract')))) {
                    require($filepath);
                }
                break;
            default:
                if (substr($file, 0, -strlen(ucfirst('interface') . '.php'))) {
                    return;
                }
                if (substr($file, strlen(ucfirst('abstract')))) {
                    return;
                }
                require($filepath);
                break;
        }

    }
}

foreach ($directories as $directory) {
    $path = sprintf('%s/%s', __DIR__, $directory);
    $files = scandir($path);
    foreach (['interface', 'abstract', 'other'] as $type) {
        foreach($files as $file) {
            loadFiles($file, $path, $type);
        }
    }
}