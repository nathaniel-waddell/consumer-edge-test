<?php

class Vehicle extends AbstractEntity
{
    /**
     * @var int
     */
    protected $vehicle_id;

    /**
     * @var string
     */
    protected $vin;

    /**
     * @var string
     */
    protected $make;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var int
     */
    protected $mileage;

    /**
     * @var float
     */
    protected $price;

    /**
     * @param int $vehicle_id
     * @return void
     */
    public function setVehicleId(int $vehicle_id): void
    {
        $this->vehicle_id = $vehicle_id;
    }

    public function getVehicleId(): int
    {
        return $this->vehicle_id;
    }

    public function setVin(string $vin): void
    {
        $this->vin = $vin;
    }

    public function getVin(): string
    {
        return $this->vin;
    }

    public function setMake(string $make): void
    {
        $this->make = $make;
    }

    public function getMake(): string
    {
        return $this->make;
    }

    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function setMileage(int $mileage): void
    {
        $this->mileage = $mileage;
    }

    public function getMileage(): int
    {
        return $this->mileage;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }
}