<?php

class Config
{
    /**
     * @var array
     */
    private array $values;

    public function __construct()
    {
        $this->values = (array)json_decode(file_get_contents(__DIR__.'/../config.json'), true);
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function get(string $key): ?string
    {
        return $this->values[$key];
    }
}