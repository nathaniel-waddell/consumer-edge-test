<?php

class Database
{
    /**
     * @var mysqli
     */
    public mysqli $connection;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $config = new Config();

        try {
            $this->connection = new mysqli(
                $config->get('db_host'),
                $config->get('db_user'),
                $config->get('db_pass'),
                $config->get('db_name')
            );
        } catch (Exception $e) {
            throw new Exception(
                'Database Exception: %s' . $e->getMessage(),
                $e->getCode(),
                $e
            );
        }

        if ($this->connection->connect_errno) {
            throw new Exception(
                'Failed to connect to MySQL: ' . $this->connection->connect_error
            );
        }

        $this->seedOnFirstRun($config);
    }

    /**
     * @param Config $config
     * @return void
     */
    private function seedOnFirstRun(Config $config): void
    {
        $result = $this->connection->query('SHOW TABLES;');

        if ($result->num_rows === 0) {
                $sql = file_get_contents(__DIR__ . '/../config/schema.sql');
                $sql = str_replace('{{db_name}}', $config->get('db_name'), $sql);
                $this->connection->multi_query($sql);
        }
    }

    /**
     * @param string $className
     * @return array
     */
    public function selectAll(string $className): array
    {
        $table = $this->getTableNameFromClassName($className);

        $query = sprintf('SELECT * FROM %s', $table);
        if ($result = $this->connection->query($query)) {
            $all =  $result->fetch_all(MYSQLI_ASSOC);
            $result->free_result();
            return $all;
        }

        $result->free_result();

        return [];
    }

    /**
     * @param string $className
     * @return int
     */
    public function countAll(string $className): int
    {
        $table = $this->getTableNameFromClassName($className);

        $query = sprintf('SELECT count(*) FROM %s', $table);
        if ($result = $this->connection->query($query)) {
            $row = $result->fetch_row();

            $result->free_result();

            return $row[0];
        }

        $result->free_result();

        return 0;
    }

    /**
     * @param string $className
     * @return string
     */
    private function getTableNameFromClassName(string $className): string
    {
        $classParts = explode('\\', $className);

        return strtolower(Strings::pluralize(end($classParts)));
    }

    /**
     * @param string $className
     * @param string $column
     * @param mixed $value
     * @return array
     */
    public function selectOne(string $className, string $column, mixed $value): array
    {
        $table = $this->getTableNameFromClassName($className);

        $query = sprintf(
            'SELECT * FROM %s WHERE %s = %s',
            $table,
            $column,
            mysqli_real_escape_string($this->connection, $value)
        );
        if ($result = $this->connection->query($query)) {
            $row =  $result->fetch_row();
            $result->free_result();
            return $row;
        }

        $result->free_result();

        return [];
    }

    /**
     * This creates in batch for more performant execution, but does not update entity ids like createNew does
     *
     * @param AbstractEntity[] $classList
     * @return void
     * @throws Exception
     */
    public function createBatch(array $classList): void
    {
        if (!count($classList)) {
            return;
        }

        $this->testIsEntity($classList[0]);
        $firstClass = $classList[0];
        $table = $this->getTableNameFromClassName($firstClass::class);

        $queries = '';
        foreach ($classList as $class) {
            $this->testIsEntity($class);
            $params = $class->toArray();

            $columns = '';
            $values = '';
            foreach ($params as $key => $value) {
                $columns .= "`$key`, ";
                $values .= '"'.mysqli_real_escape_string($this->connection, $value).'", ';
            }

            $queries .= sprintf(
                'INSERT INTO %s (%s) VALUES (%s); ',
                $table,
                substr($columns, 0, -2),
                substr($values, 0, -2)
            );
        }

        $this->connection->multi_query($queries);
    }

    /**
     * @param object $class
     * @return void
     * @throws Exception
     */
    private function testIsEntity(object $class)
    {
        if (!($class instanceof AbstractEntity)) {
            throw new Exception(sprintf('%s is not an instance of AbstractEntity.', $class::class));
        }
    }

    /**
     * @param AbstractEntity $class
     * @return void
     */
    public function createNew(AbstractEntity $class): void
    {
        $table = $this->getTableNameFromClassName($class::class);
        $params = $class->toArray();

        $columns = '';
        $values = '';
        foreach ($params as $key => $value) {
            $columns .= "`$key`, ";
            $values .= '"'.mysqli_real_escape_string($this->connection, $value).'", ';
        }

        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s);',
            $table,
            substr($columns, 0, -2),
            substr($values, 0, -2)
        );

        $this->connection->query($query);

        $class->setId($this->connection->insert_id);
    }
}