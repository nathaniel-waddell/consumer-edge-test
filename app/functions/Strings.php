<?php

class Strings
{
    /**
     * Pluralizes a word if quantity is not one.
     *
     * @param string $singular Singular form of word
     * @param string|null $plural Plural form of word; function will attempt to deduce plural form from singular if not provided
     * @param int $quantity Number of items - default is 2 when just using to pluralize a known singular
     * @return string Pluralized word if quantity is not one, otherwise singular
     */
    public static function pluralize(string $singular, string $plural = null, int $quantity = 2): string
    {
        if ($quantity == 1 || !strlen($singular)) {
            return $singular;
        }

        if ($plural !== null) {
            return $plural;
        }

        $last_letter = strtolower($singular[strlen($singular) - 1]);
        return match ($last_letter) {
            'y' => substr($singular, 0, -1) . 'ies',
            's' => $singular . 'es',
            default => $singular . 's',
        };
    }

    /**
     * A really rough implementation of a singularization function
     *
     * @param string $plural
     * @return string
     */
    public static function singularize(string $plural): string
    {
        return substr($plural, 0, -1);
    }

    /**
     * @param string $input
     * @param string $extractAfterThis
     * @param string $extractUntilThis
     * @return bool|string
     */
    public static function getFromBetweenTwoStrings(string $input, string $extractAfterThis, string $extractUntilThis)
    {
        $string = ' ' . $input;
        $ini = strpos($string, $extractAfterThis);

        if ($ini == 0) {
            return '';
        }

        $ini += strlen($extractAfterThis);
        $len = strpos($string, $extractUntilThis, $ini) - $ini;

        return substr($string, $ini, $len);
    }
}