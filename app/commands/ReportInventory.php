<?php

class ReportInventory
{
    /**
     * @var bool
     */
    private bool $countOnly = false;

    private string $tableName = '';

    /**
     * @var AbstractEntity
     */
    private AbstractEntity $class;

    /**
     * @param array $arguments
     * @throws Exception
     */
    public function __construct(array $arguments)
    {
        if (!array_key_exists(2, $arguments)) {
            throw new \Exception('Please enter the desired table you wish to report.');
        }

        $this->tableName = $arguments[2];
        $className = ucfirst(Strings::singularize($this->tableName));

        try {
            $this->class = new $className;
        } catch (Error $e) {
            throw new \Exception('Please enter a valid table you wish to report.');
        }

        $this->countOnly = array_key_exists(3, $arguments) && $arguments[3] === 'count';

        $this->database = new Database();
    }

    /**
     * @return string
     */
    public function report(): string
    {
        if ($this->countOnly) {
            return sprintf('Table %s has %d records!',
                $this->tableName,
                $this->database->countAll($this->class::class)
            );
        }

        $data = $this->database->selectAll($this->class::class, $this->countOnly);

        return json_encode($data);
    }
}