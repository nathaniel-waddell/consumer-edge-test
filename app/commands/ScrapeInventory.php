<?php

class ScrapeInventory
{
    const DEFAULT_NO_PAGES_TO_CRAWL = 1;

    /**
     * @var Database
     */
    private $database;

    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @var int
     */
    private $startPage;

    /**
     * @var int
     */
    private $endPage;

    private $pagesToCrawl = 0;

    /**
     * @param array $arguments
     * @throws Exception
     */
    public function __construct(array $arguments)
    {
        if (!array_key_exists(2, $arguments)) {
            throw new \Exception('Please enter the desired provider you wish to scrape.');
        }
        $provider = ucfirst($arguments[2]);
        $this->startPage = array_key_exists(3, $arguments) ? $arguments[3] : 1;
        $this->endPage = array_key_exists(4, $arguments) ? $arguments[4] : 1;

        $this->database = new Database();

        try {
            $this->provider = new $provider;
        } catch (\Exception $e) {
            throw new \Exception ('Invalid provider given.');
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function scrape(): string
    {
        $this->setupPageRanges();

        $count = 0;
        for ($i = 0; $i < $this->pagesToCrawl; $i++) {
            $inventory = $this->provider->fetchInventoryByPage($this->startPage);
            $this->database->createBatch($inventory);
            $count += count($inventory);
            ++$this->startPage;
        }

        return sprintf('Successfully scraped %d vehicles!', $count);
    }

    /**
     * @return void
     */
    private function setupPageRanges(): void
    {
        if ($this->endPage === 1) {
            $this->pagesToCrawl = self::DEFAULT_NO_PAGES_TO_CRAWL;
            return;
        }

        if ($this->startPage > $this->endPage) {
            throw new \LogicException('startPage cannot be greater than endPage.');
        }

        $this->pagesToCrawl = $this->endPage - $this->startPage + 1;
    }
}